#!/usr/bin/env bash

## Following instructions from https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-on-ubuntu-14-04

apt-get update

apt-get install nginx

apt-get install mysql-server
mysql_install_db
mysql_secure_installation
# N for first, hit Enter for Y for the rest

apt-get install php5-fpm php5-mysql
curl "https://bitbucket.org/SeanJohnno/lemp/raw/38da04b425e6ca6dd2ae65a619bd0de768809032/php.ini" > /etc/php5/fpm/php.ini
service php5-fpm restart

curl "https://bitbucket.org/SeanJohnno/lemp/raw/38da04b425e6ca6dd2ae65a619bd0de768809032/default" > /etc/nginx/sites-available/default
